express = require 'express'
socketio = require 'socket.io'
parseCookie = require('connect').utils.parseCookie
RedisStore = require('connect-redis')(express)
db = require('./db')
_ = require('underscore')

logger = require './logging'

ICONS = ["beer", "bicycle", "canoe", "cat", "dog", "flower", "log_cabin", "rollerscate", "star", "tree"]

start = (options={port: 8000, host: 'localhost'}) ->
  sessionStore = new RedisStore
  app = express.createServer()
  
  #
  # Config
  #
  app.logger = logger # debug/dev logging
  app.configure ->
    app.use require('connect-assets')()
    app.use express.logger()
    app.use express.bodyParser()
    app.use express.cookieParser()
    app.use express.session
      secret: "shhhhhhhhhhh don't tell"
      key: 'express.sid'
      store: sessionStore

  app.configure 'development', ->
    app.use express.static __dirname + '/../static'
    app.use express.errorHandler { dumpExceptions: true, showStack: true }

  app.configure 'production', ->
    app.use express.static __dirname + '/../static', { maxAge: 1000*60*60*24 }

  app.set 'view engine', 'jade'
  app.set 'view options', layout: false

  app.get '/:room', (req, res) ->
    res.render 'index',
      title: "10 Points for #{req.param("room")}"

  app.get '/', (req, res) ->
    res.render 'index',
      title: "Consensus meeting: 10 Points"

  require('./auth').route(app, options.host)
  app.listen options.port

  #
  # Socket sessions
  #
  room_sessions = {}
  io = socketio.listen(app)
  io.set 'log level', 0
  io.set 'authorization', (data, accept) ->
    if data.headers.cookie
      data.cookie = parseCookie(data.headers.cookie)
      data.sessionID = data.cookie['express.sid']
      data.sessionStore = sessionStore
      sessionStore.get data.sessionID, (err, session) ->
        if err or not session
          accept err?.message or "Error", false
        else
          data.session = session
          accept null, true
    else
      return accept 'No cookie transmitted', false

  room_sessions = {}
  session_rooms = {}
  io.of('/bor').on 'connection', (socket) ->
    sessionID = socket.handshake.sessionID
    session = socket.handshake.session
    socket.join socket.handshake.sessionID
    unless session_rooms[sessionID]?
      session_rooms[sessionID] = {}

    # Emit a list of all current users.
    emit_users_of = (room) ->
      session_ids = []
      for session_id, count of room_sessions[room].sockets
        if count > 0
          session_ids.push(session_id)
      users = []
      for id in session_ids
        users.push room_sessions[room].icons[id]
      socket.broadcast.to(room).emit 'users', users
      socket.emit 'users', users

    #
    # Join room
    #
    socket.on 'join_room', (data) ->
      unless data.room
        return

      socket.join(data.room)

      # Create the room
      unless room_sessions[data.room]
        room_sessions[data.room] =
          icons: {}
          icon_usage: {}
          sockets: {}
        for icon in ICONS
          room_sessions[data.room].icon_usage[icon] = 0
      room = room_sessions[data.room]
      
      # Add name to set of active room sessions
      unless session_rooms[sessionID][data.room]?
        session_rooms[sessionID][data.room] = 0
      session_rooms[sessionID][data.room] += 1

      # Choose an icon
      unless room.icons[sessionID]
        least_used = null
        min_usage = 10000000
        # argmin
        for icon, usage of room.icon_usage
          if usage < min_usage
            min_usage = usage
            least_used = icon
        room.icons[sessionID] = least_used
        room.icon_usage[least_used] += 1

      # Increment socket count for room
      if room.sockets[sessionID]
        room.sockets[sessionID] += 1
      else
        room.sockets[sessionID] = 1

      socket.emit 'icon', icon: room.icons[sessionID]
      emit_users_of(data.room)
      # Announce on first socket from a session.
      if session_rooms[sessionID][data.room] == 1
        socket.emit 'you_joined', room: data.room
        socket.broadcast.to(data.room).emit 'they_joined', icon: room.icons[sessionID]
      db.get "shared_rights:#{data.room}", (err, rights) ->
        if err?
          rights = ({message: "", editors: [], votes: []} for i in [0...10])
          db.set "shared_rights:#{data.room}", rights, (err) ->
            if err then logger.error("Error writing: #{err}")
        socket.emit('shared_rights', rights)

    socket.on 'right_change', (data) ->
      db.get "shared_rights:#{data.room}", (err, rights) ->
        if err?
          return logger.error("right_change: #{data.room} Error reading data: #{err}")
        switch data.type
          when "message"
            rights[data.index].message = data.message
            rights[data.index].editors = data.editors
            rights[data.index].votes   = data.votes
          when "add_vote"
            unless rights[data.index].votes?
              rights[data.index].votes = []
            unless _.contains rights[data.index].votes, data.voter
              rights[data.index].votes.push(data.voter)
          when "remove_vote"
            if rights[data.index].votes?
              rights[data.index].votes = _.reject rights[data.index].votes, (a) -> a == data.voter
          when "start_editing"
            unless rights[data.index].editors?
              rights[data.index].editors = []
            unless _.contains rights[data.index].editors, data.editor
              rights[data.index].editors.push(data.editor)
          when "stop_editing"
            if rights[data.index].editors?
              rights[data.index].editors = _.reject rights[data.index].editors, (a) -> a == data.editor
        db.set "shared_rights:#{data.room}", rights, (err) ->
          logger.error("Error writing data #{err}") if err?
      socket.broadcast.to(data.room).emit 'right_change', data

    #
    # Leave room
    #
    leave_room = (name) ->
      session_rooms[sessionID][name] -= 1
      if session_rooms[sessionID][name] == 0
        delete session_rooms[sessionID][name]
      room_sessions[name].sockets[sessionID] -= 1
      if room_sessions[name].sockets[sessionID] == 0
        icon = room_sessions[name].icons[sessionID]
        # Tell folks they left.
        socket.broadcast.to(name).emit 'they_left', icon: icon
        # Remove from session.
        delete room_sessions[name].sockets[sessionID]
        # Remove from any active editor list.
        db.get "shared_rights:#{name}", (err, data) ->
          if (err) then return logger.error("leave_room: Error reading data: #{err}")
          for i in [0...data.length]
            right = data[i]
            if _.contains right.editors, icon
              right.editors = _.reject right.editors, (a) -> a == icon
              socket.broadcast.to(name).emit 'right_change',
                room: name
                index: i
                type: 'stop_editing'
                editor: icon
          db.set "shared_rights:#{name}", data, (err) ->
            logger.error("Error writing data #{err}") if err?
        emit_users_of(name)

    socket.on 'disconnect', ->
      socket.leave(name)
      for name, count of session_rooms[sessionID]
        leave_room(name)

module.exports = { start }
