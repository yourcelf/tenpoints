redis = require 'redis'

PREFIX = 'billofrights:'

client = redis.createClient()

set = (key, data, callback) ->
  client.set PREFIX + key, JSON.stringify(data), (err, response) ->
    callback? err

get = (key, callback) ->
  client.get PREFIX + key, (err, response) ->
    throw err if err?
    if response != null
      data = JSON.parse(response)
      callback null, data
    else
      callback 'key not found'

del = (key, callback) ->
  client.del PREFIX + key, (err, response) ->
    throw err if err?
    if callback
      if response == 1
        callback null
      else
        callback 'key not found'

module.exports = { set, get, del }
