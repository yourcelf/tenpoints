#= require jquery
#= require underscore
#= require underscore-autoescape
#= require backbone

# Safety.
unless window.console?.log?
  window.console =
    log: ->
    error: ->

$.fn.flash = (times, duration, color, callback) ->
  T = this
  unless times? then times = 3
  unless duration? then duration = 200
  unless color? then color = "#f90"
  bg = T.css("background-color")
  T.css("background-color", color)
  for i in [0...times]
    do (i) ->
      setTimeout ->
        T.fadeOut duration, ->
          T.fadeIn duration, ->
      , i*duration*2+50
  # finish.
  setTimeout ->
    T.css("background-color", bg)
    callback() if callback?
  , times * duration*2 + 50


#
# Flash
#
class FlashMessage extends Backbone.Model
class FlashMessageList extends Backbone.Collection
  model: FlashMessage

class FlashView extends Backbone.View
  template: _.template $("#flashView").html()
  tagName: 'li'
  events:
    'click .close': 'closeMessage'
  initialize: (flashModel) ->
    @model = flashModel
  render: =>
    $(@el).html @template message: @model.get "message"
    $(@el).addClass @model.get "level"
    if @model.get("level") == "info"
      setTimeout =>
        @closeMessage()
      , 4000
    this
  closeMessage: (event) =>
    @trigger "close", @model
    $(@el).remove()
    return false

class FlashListView extends Backbone.View
  tagName: 'ul'
  initialize: (flashList) ->
    @flashList = flashList
    @flashList.on "add", (model) =>
      fv = new FlashView(model)
      fv.on "close", (model) =>
        @flashList.remove(model)
        return false
      $(@el).append fv.render().el

# One global flash list:
window.flashList = new FlashMessageList
$("#flash").html new FlashListView(flashList).render().el
# Add to global flash list:
window.flash = (level, message) ->
  model = new FlashMessage {level, message}
  flashList.add(model)

#
# Bill of rights stuff.
#
views = []
class RightView extends Backbone.View
  template: _.template $("#rightView").html()
  events:
    'click input.save': 'save'
    'click a.toggle-vote': 'toggleVote'
    'click a.edit': 'edit'
  
  initialize: (index, room_name, self_icon) ->
    @index = index
    @self_icon = self_icon
    @room_name = room_name
    @message = ""
    @votes = []
    @editors = []
    
  render: =>
    $(@el).html @template
      index: @index + 1
      voted: _.contains @votes, @self_icon
    $(@el).addClass "right"
    @update()
    this

  updateVoteCount: =>
    if @votes.length != 1
      @$(".count").html("#{@votes.length} supporters ")
    else
      @$(".count").html("1 supporter ")

  update: =>
    @$(".finished .text").html _.escapeHTML(@message)
    @updateVoteCount()
    @$(".votes").html ""
    for name in @votes
      @$(".votes").append small_icon(name)

    @$(".toggle-vote, .votes, .count").toggle(@message != "")
    @$(".toggle-vote").toggleClass("active", _.contains(@votes, @self_icon))

    if @message == ""
      $("a.edit", @el).hide()
      $(".editor", @el).show()
    else
      val = $("textarea", @el).val()
      if @message == val or val == ""
        $(".editor", @el).hide()
      $("a.edit", @el).show()
    if $(".editor", @el).is(":visible")
      $("a.edit", @el).html("Cancel")
    else
      $("a.edit", @el).html("Edit")

    @$(".editors").html("")
    @addEditor(editor) for editor in @editors

  save: =>
    if $("textarea", @el).val() == @message
      if @message != ""
        $("a.edit", @el).click()
      return false
    if (@votes.length == 0 or
        @votes.length == 1 and @votes[0] == @self_icon or
        confirm "Are you sure?  This will erase #{@votes.length} vote#{if @votes.length > 1 then "s" else ""}.")
      @message = $("textarea", @el).val()
      if @message != ""
        @votes = [@self_icon]
      else
        @votes = []
      @editors = _.reject @editors, (a) => a == @self_icon
      @trigger "message", this
      @update()
    return false

  edit: (event) =>
    @$("textarea").val $.trim @$(".finished .text").text()
    editor = @$(".editor")
    if editor.is(":visible")
      editor.hide()
      $(event.currentTarget).html("Edit")
      @editors = _.reject @editors, (a) => a == @self_icon
      @trigger "stop_editing", this
    else
      editor.show()
      $(event.currentTarget).html("Cancel")
      unless _.contains @editors, @self_icon
        @editors.push @self_icon
      @trigger "start_editing", this
    return false

  toggleVote: =>
    if _.contains @votes, @self_icon
      @votes = _.reject @votes, (a) => a == @self_icon
      @trigger "remove_vote", this
    else
      @votes.push @self_icon
      @trigger "add_vote", this
    @update()
    return false

  addEditor: (icon) =>
    unless @$(".editors img[data-icon=#{icon}]").length
      @$(".editors").append $("<img src='/img/pencil.gif' data-icon='#{icon}' title='#{icon} is editing' />")
    @editors.push icon

  removeEditor: (icon) =>
    @$(".editors img[data-icon=#{icon}]").remove()
    @editors = _.reject @editors, (a) -> a == icon

  addVote: (icon) =>
    vote_img = $(small_icon(icon))
    @$(".votes").append vote_img
    @votes.push icon
    console.log @votes
    @updateVoteCount()
    vote_img.flash(3, 200, "#0f0")

  removeVote: (icon) =>
    vote_img = @$(".votes img[data-icon=#{icon}]")
    vote_img.flash 3, 200, "#f00",=>
      vote_img.remove()
      @votes = _.reject @votes, (a) -> a == icon
      console.log @votes
      @updateVoteCount()

  loudUpdate: =>
    @update()
    @$el.flash(1, 1000)
    console.log @$el

#
# Socket IO
#
self_icon = null
room_name = null
socket = io.connect("/bor")
icon = (name) -> "<img src='/img/icons/#{name}.png' alt='#{name}' data-icon='#{name}' />"
small_icon = (name) -> "<img src='/img/icons/#{name}_small.png' alt='#{name}' data-icon='#{name}' />"
rights_views = []

socket.on 'users', (data) ->
  icons = (small_icon(name) for name in data)
  $("#auth .online").html("Online now: #{icons.join(" ")}")
socket.on 'icon', (data) ->
  self_icon = data.icon
  for view in rights_views
    view.self_icon = self_icon
  $("#auth .you").html("You are: <span class='you'>#{small_icon(self_icon)}</span>")
socket.on 'they_joined', (data) ->
  flash 'info', "#{icon(data.icon)} has joined."
socket.on 'they_left', (data) ->
  flash 'info', "#{icon(data.icon)} has left."
socket.on 'you_joined', (data) ->
  $("#intro").hide()
  flash 'info', "You've joined \"#{data.room}\""

socket.on 'shared_rights', (data) ->
  console.log "shared_rights", data
  # Initial load only.
  for i in [0...data.length]
    rights_views[i].message = data[i].message or ""
    rights_views[i].votes = data[i].votes or []
    rights_views[i].editors = data[i].editors or []
    rights_views[i].update()

socket.on 'right_change', (data) ->
  console.log data
  switch data.type
    when "message"
      rights_views[data.index].message = data.message
      rights_views[data.index].votes = data.votes
      rights_views[data.index].editors = data.editors
      rights_views[data.index].loudUpdate()
    when "add_vote"
      rights_views[data.index].addVote(data.voter)
    when "remove_vote"
      rights_views[data.index].removeVote(data.voter)
    when "start_editing"
      rights_views[data.index].addEditor(data.editor)
    when "stop_editing"
      rights_views[data.index].removeEditor(data.editor)

joinRoom = (room_name) ->
  count = 10
  #
  # Set up views.
  #
  for i in [0...count]
    view = new RightView(i, room_name, self_icon)
    do (view) ->
      rights_views[i] = view
      view.render()

      emit_update = (data) ->
        data.room = room_name
        socket.emit 'right_change', data

      view.on 'message', (view) -> emit_update
        type: "message"
        index: view.index
        message: view.message
        editors: view.editors
        votes: view.votes
      view.on 'add_vote', (view) -> emit_update
        type: "add_vote"
        index: view.index
        voter: view.self_icon
      view.on 'remove_vote', (view) -> emit_update
        type: "remove_vote"
        index: view.index
        voter: view.self_icon
      view.on 'start_editing', (view) -> emit_update
        type: "start_editing"
        index: view.index
        editor: view.self_icon
      view.on 'stop_editing', (view) -> emit_update
        type: "stop_editing"
        index: view.index
        editor: view.self_icon

  #
  # Add in two table columns.  NOTE: depends on even number of points.
  #
  table = $("#rights table")
  table.html("")
  for i in [0...count/2]
    table.append $("<tr>").append(
      $("<td>").append(rights_views[i].el)
    ).append(
      $("<td>").append(rights_views[i + count/2].el)
    )

  socket.emit 'join_room', room: room_name

if window.location.pathname.length > 1
  joinRoom(window.location.pathname.substring(1))
else
  $("#intro").show()
  $("#joiner").submit ->
    room_name = $("#joiner input[type=text]").val().toLowerCase().replace(/[^-_.a-z0-9]/g, '-')
    window.location.href = "/#{room_name}"
    return false

# DEBUG
do ->
  showsize = -> $('#size').html $(window).width() + " x " + $(window).height()
  $(window).on 'resize', showsize
  showsize()

